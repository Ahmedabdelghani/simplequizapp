# Description
This is a simple quiz app that downloads a certain xml file from a Dopbox account to quiz the user. Once the user is finished, a student name and password can be entered before emailing the answers to a preselected email.

# Operation
Once the app is running, you will have to answer 10 multiple choice questions to the best of your ability. When you reach the last question, you will have the option to enter your name and student number. Then you may tap on the email icon and use your favorite email app to send an xml string with your answers. The recipient will automatically be entered.
The app should work in landscape and portrait modes without resetting your answers. 

# Bugs

- The app occasionally resets buttons when going back and forth between questions. No certain pattern is found yet to recreate the bug.
- The app requires a 3 second delay when starting it in order to display the first question properly

# Future Work

- Email the list of answers as an xml file instead of a string
- Create a feature to identify the correct answers for the user once the email has been sent