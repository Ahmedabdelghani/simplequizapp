package com.example.ldnel.myapplication;

/*
This example opens a hardcoded raw file and parses the XML data
It presents a user interface to scroll through multiple choice test questions
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class QuizActivity extends AppCompatActivity {

    private  final String TAG = this.getClass().getSimpleName() + " @" + System.identityHashCode(this);
    private static final String STATE_questionIndex = "STATE_questionIndex";
    private static final String STATE_radioGroup = "STATE_radioGroup";

    private static final String fileURLString = "https://www.dropbox.com/s/hfyeojq6ykrbyse/questions?dl=1";
    private static final String downloadedQuestionFileName = "questionsFile.xml";

    private Button mNextButton;
    private Button mPrevButton;
    private RadioGroup radioGroup;
    private FloatingActionButton fab;

    //model state variables
    private TextView mQuestionTextView;
    private RadioButton option1;
    private RadioButton option2;
    private RadioButton option3;
    private RadioButton option4;
    private RadioButton option5;
    private EditText    nameField;
    private EditText    studentNumberField;
    private ArrayList<Question> mQuestions;
    private int mQuestionIndex;
    private int questionNumber = 0;
    private String answerText;
    private String xmlText = "";

    private ArrayList<String> selectedAnswers = new ArrayList<>();

    private void updateUI() throws InterruptedException {
        //update UI views based on state of model objects

        if (questionNumber == 0) {
            mPrevButton.setVisibility(View.INVISIBLE);
            fab.setVisibility(View.INVISIBLE);
            nameField.setVisibility(View.INVISIBLE);
            studentNumberField.setVisibility(View.INVISIBLE);
        } else if (questionNumber == mQuestions.size()) {
            mNextButton.setVisibility(View.INVISIBLE);
            fab.setVisibility(View.VISIBLE);
            nameField.setVisibility(View.VISIBLE);
            studentNumberField.setVisibility(View.VISIBLE);
        } else {
            mPrevButton.setVisibility(View.VISIBLE);
            mNextButton.setVisibility(View.VISIBLE);
            fab.setVisibility(View.INVISIBLE);
            nameField.setVisibility(View.INVISIBLE);
            studentNumberField.setVisibility(View.INVISIBLE);
        }
        if(mQuestions != null && mQuestions.size() > 0) {

            if(mQuestionIndex > mQuestions.size()) mQuestionIndex = 0;

            mQuestionTextView.setText("" + (mQuestionIndex + 1) + ") " +
                    mQuestions.get(mQuestionIndex).toString());

            option1.setChecked(false);
            option2.setChecked(false);
            option3.setChecked(false);
            option4.setChecked(false);
            option5.setChecked(false);

            if (mQuestions.size() > 1 ) {
                option1.setText("" + mQuestions.get(mQuestionIndex).getAnswers().get(0));
                option2.setText("" + mQuestions.get(mQuestionIndex).getAnswers().get(1));
                option3.setText("" + mQuestions.get(mQuestionIndex).getAnswers().get(2));
                option4.setText("" + mQuestions.get(mQuestionIndex).getAnswers().get(3));
                option5.setText("" + mQuestions.get(mQuestionIndex).getAnswers().get(4));

                for (int i = 0; i < selectedAnswers.size(); i++) {
                    if (option1.getText().toString().equals(selectedAnswers.get(i))) {
                        option1.setChecked(true);
                    }

                     else if (option2.getText().toString().equals(selectedAnswers.get(i))) {
                        option2.setChecked(true);
                    }

                    else if (option3.getText().toString().equals(selectedAnswers.get(i))) {
                        option3.setChecked(true);
                    }

                    else if (option4.getText().toString().equals(selectedAnswers.get(i))) {
                        option4.setChecked(true);
                    }

                    else if (option5.getText().toString().equals(selectedAnswers.get(i))) {
                        option5.setChecked(true);
                    }
                }
                int size = selectedAnswers.size();
            }
        }
    }

    //Activity Lifecycle Transitions
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate(Bundle)");
        //activity exists, is stopped and not visible to user

        setContentView(R.layout.activity_quiz); //set and inflate UI to manage

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                String studentName = "";
                String studentNumber = "";
                String answerBeginTag = "<answer>";
                String answerEndTag = "</answer>";
                String nameBeginTag = "<student_name>";
                String nameEndTag = "</student_name>";
                String numberBeginTag = "<student_number>";
                String numberEndTag = "</student_number>";
                String currentAnswer;
                String email = Exam.getEmail();

                studentName = nameField.getText().toString();
                studentNumber = studentNumberField.getText().toString();
                String emailSubject =studentName +  " - Exam Answers";
                xmlText = nameBeginTag + studentName + nameEndTag + numberBeginTag + studentNumber + numberEndTag;


                for (int i = 0; i < selectedAnswers.size(); i++) {
                    currentAnswer = selectedAnswers.get(i);
                    xmlText = xmlText + answerBeginTag + currentAnswer + answerEndTag;
                }
                System.out.println(xmlText);

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + email));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, emailSubject);
                emailIntent.putExtra(Intent.EXTRA_TEXT, xmlText);
                if (!email.equals("")) {
                    startActivity(Intent.createChooser(emailIntent, "Email Client ..."));
                }
            }
        });

        mPrevButton = (Button) findViewById(R.id.prev_button);
        mNextButton = (Button) findViewById(R.id.next_button);

        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);
        mQuestionTextView.setTextColor(Color.BLUE);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        option1 = (RadioButton) findViewById(R.id.option1);
        option2 = (RadioButton) findViewById(R.id.option2);
        option3 = (RadioButton) findViewById(R.id.option3);
        option4 = (RadioButton) findViewById(R.id.option4);
        option5 = (RadioButton) findViewById(R.id.option5);

        nameField = (EditText) findViewById(R.id.edit_name);
        studentNumberField = (EditText) findViewById(R.id.edit_sNumber);

        mPrevButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.i(TAG, "Prev Button Clicked");

                if(mQuestionIndex > 0) mQuestionIndex--;
                if(questionNumber > 0)
                    questionNumber--;

                    try {
                        updateUI();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                //for debugging
            }
        });

        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Option 1 chosen");
                answerText = option1.getText().toString();
                selectedAnswers.set(questionNumber, answerText);
                option1.setChecked(true);

            }
        });

        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Option 2 chosen");
                answerText = option2.getText().toString();
                selectedAnswers.set(questionNumber, answerText);
                option2.setChecked(true);

            }
        });

        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Option 3 chosen");
                answerText = option3.getText().toString();
                selectedAnswers.set(questionNumber, answerText);
                option3.setChecked(true);
            }
        });

        option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Option 4 chosen");
                answerText = option4.getText().toString();
                selectedAnswers.set(questionNumber, answerText);
                option4.setChecked(true);
            }
        });

        option5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Option 5 chosen");
                answerText = option5.getText().toString();
                selectedAnswers.set(questionNumber, answerText);
                option5.setChecked(true);
            }
        });

        radioGroup.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.i(TAG, "Next Button Clicked");
                onRadioButtonClicked(v);
            }

        });

        mNextButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Log.i(TAG, "Next Button Clicked");

                if(mQuestionIndex < mQuestions.size()-1) mQuestionIndex++;
                if (questionNumber < mQuestions.size()) questionNumber++;

                    try {
                        updateUI();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

            }

        });

        //Initialise Data Model objects
        mQuestionIndex = 0;

        if(isNetworkAvailable()) {
            Log.i(TAG, "starting file download Task");
            FileDownloadTask download = new FileDownloadTask();
            download.execute();
        }


        //recover state from saved instance state
        if(savedInstanceState != null) mQuestionIndex = savedInstanceState.getInt(STATE_questionIndex);
        if(savedInstanceState != null) mQuestionIndex = savedInstanceState.getInt(STATE_questionIndex);

        try {
            Thread.sleep(3000);
            updateUI(); //update the UI view components
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Helper method to determine if Internet connection is available.
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void onRadioButtonClicked(View view) {
        Log.i(TAG, "Question Answered");
        int selectedId = radioGroup.getCheckedRadioButtonId();

        RadioButton answer = (RadioButton) findViewById(selectedId);
        String answerText = answer.getText().toString();
        selectedAnswers.add(answerText);
        Log.i(TAG, answer.getText().toString());
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.i(TAG, "onStart()");
        //activity paused but visible to user, not in foreground

    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.i(TAG, "onResume()");
        //activity is visible, running and in foreground

    }
    @Override
    protected void onPause(){
        super.onPause();
        Log.i(TAG, "onPause()");
        //activity paused, visible, not in foreground

    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.i(TAG, "onStop()");
        //activity stopped, not visible
    }

    protected void onDestroy(){
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
        //activity and will be destroyed
    }

    //Overide saving and restoring of activity instance state data
    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState){
        //called after onPause()
       super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState()");
        //save current question index.
        savedInstanceState.putInt(STATE_questionIndex, mQuestionIndex);
        savedInstanceState.putStringArrayList(STATE_radioGroup, selectedAnswers);

    }

    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState){
        //Gets called after onStart();
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(TAG, "onRestoreInstanceState()");
        //recover state from saved instance state
        if(savedInstanceState != null) mQuestionIndex = savedInstanceState.getInt(STATE_questionIndex);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class FileDownloadTask extends AsyncTask <Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                FileOutputStream fileOutputStream = openFileOutput(downloadedQuestionFileName, Context.MODE_PRIVATE);
                FileDownloader.DownloadFromUrl(fileURLString, fileOutputStream);
            } catch (FileNotFoundException e) {
                Log.i(TAG, "ERROR: file to download not found");
                e.printStackTrace();
            }

            //parse the questions from included raw file resource
            try {
                FileInputStream iStream = QuizActivity.this.openFileInput(downloadedQuestionFileName);
                BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));

                ArrayList<Question> parsedModel = Exam.pullParseFrom(reader);
                if(parsedModel == null || parsedModel.isEmpty())
                    Log.i(TAG, "ERROR: Question File Not Parsed");
                mQuestions = parsedModel;
                for (int i = 0; i < mQuestions.size(); i++) {
                    selectedAnswers.add("");
                }

                reader.close();

            }
            catch(java.io.FileNotFoundException e){
                Log.i(TAG, "ERROR: downloaded file not found");
            }
            catch(java.io.IOException e){
                Log.i(TAG, "ERROR: unable to close file");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result){

        }
    }

}
